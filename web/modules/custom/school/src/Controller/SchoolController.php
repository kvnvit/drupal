<?php

/*  Controller class for School module.
 * Implements methods for base routes.
 */

namespace Drupal\school\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SchoolController extends ControllerBase {

  /**
   * Current user.
   */
  protected $currentUser;

  /**
   * Constructs a new Controller object.
   *
   * @param AccountInterface $current_user
   * Current user
   */
  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * Basic function handles controller's page view.
   *
   * @return string[]
   */

  public function viewTest() {

//    $variable1 = some_function1();
//    $variable2 = some_function2();
//    $hello = \Drupal::service('hello.my_service');

    return [
      '#theme' => 'school_template',
      '#variable1' => 'Hello', // $variable1
      '#variable2' => $this->currentUser->getAccountName(), // $variable2
//      '#markup' => $hello->helloWorld(),
    ];
  }

}
