<?php

namespace Drupal\school;

/**
 * Provides my custom service.
 */
class HelloService {

  /**
   * Gets hello message.
   *
   * @return string
   *   The message.
   */
  public function helloWorld() {
    return 'Hello from my custom service, World!';
  }

}
