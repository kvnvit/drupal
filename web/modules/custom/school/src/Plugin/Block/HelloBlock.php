<?php

namespace Drupal\school\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "hello_block",
 *   admin_label = @Translation("Hello block"),
 *   category = @Translation("Hello World"),
 * )
 */
class HelloBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $container = \Drupal::getContainer();
    $user_name = $container->get('current_user')->getAccountName();

    return [
      '#markup' => $this->t('Block: Hello, ' . $user_name),
    ];
  }

}
